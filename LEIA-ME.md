# LEIA-ME #
Este Leia-me Documenta tudo que foi feito neste projeto.
Projeto LP2 Consumo de APIs
Vinicius Ferreira Alves de Oliveira
Prontuario:GU3002519

### Para que é este repositorio? ###
* Utilizar API e sua característica a seu favor
* Implementar uma API desiginada em sorteio na aplicação do seu site
* API utilizada: People(Google)
* Função:Criar,listar,alterar e excluir contatos da sua agenda google em usuarios autenticados.
* (https://bitbucket.org/ViniciusFerreira365/vinicius/src/master/)

### Processos de Implentação ###
* 1° Criar uma conta Google
* 2° Ativar a chave de autorização e configuir a tela de consentimento OAuth.2.0
* 3° Baixar a biblioteca cliente e configurar o codeigniter
* 4° Baixar arquivos JSON do console.dev.google
* 5° Criar paginas do projeto Responsivas em MVC
* 6° Implementar a API com base em pesquisas e leitura da Documentação
* 7° A documentação é boa, ela foi implementada em pagina PHP e pode ser usada em linha de comando com requisições em REST HTTP, a biblioteca em php está em fase beta pode
  ocorrer alteraçoes logo toda semada baixava outra bibilioteca do Git da google
* 8° Fora este detalhe tudo ocorreu como o esperado, por conta de um maior tempo na  data de entrega foi possivel colocar a API em na pagina PHP

### QuickStar ###
Esta API possiu um arquivo para visualizar o que é feito para isso segue os seguntes passos:
1°Entre na pasta application via terminal pelo comando cd application
2° execute no terminal(linha de comando) php Peoples_API_Quickstart
3° Autentique a conta(agendaifspgru@gmail.com senha: ifspgru123) e Cole o link gerado no navegador no terminal
4° Veja seus contatos no terminal

### QuickStar PHP ###
1° clique na pagina resultado API
2° Vai ocorrer um erro do php e vai aparecer um link para autenticar a conta , autentique a conta agendaifspgru@gmail.com senha: ifspgru123, copie e cole o link gerado na resultado api
3° Veja os contatos na pagina.

