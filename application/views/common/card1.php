<!-- Card -->
<div class="view overlay zoom">
    <div class="card testimonial-card">
  <!-- Background color -->
      <div class="card-up indigo lighten-1"></div>

  <!-- Avatar -->
  <div class="avatar mx-auto white ">
    <img src=" <?php echo base_url('assets/img/perfil.jpg');?>" class="img-fluid rounded mx-auto d-block" alt="woman avatar">
  </div>
 

  <!-- Content -->
  <div class="card-body mx-auto">
    <!-- Name -->
    <h4 class="card-title text-center"> <strong>Vinicius Ferreira Alves de Oliveira</strong></h4>
    <hr>
    <!-- Quotation -->
    <p class="card-text text-dark"><em>Prontuario:GU3002519.</em></p>
    <p class="card-text text-dark"><em>Email: v.fereira@aluno.ifsp.edu.br</em></p>
  </div>

      </div>
    </div>
<!-- Card -->
 