<!-- Card -->
<div class="card card-cascade wider reverse">

  <!-- Card image -->
  <div class="view overlay zoom">
  <div class="view view-cascade overlay">
    <img class="img-fluid" src="https://portal.ifspguarulhos.edu.br/images/logos/Guarulhos-02.jpg" alt="Card image cap"> 
    <a href="#!">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center">

    <!-- Title -->
    <h4 class="card-title"><strong>Projeto LP2 Consumo de APIs</strong></h4>
    <!-- Subtitle -->
    <h6 class="font-weight-bold indigo-text py-2">Agenda IFSP</h6>
    <!-- Text --> 
  </div>

  </div>
</div>
<!-- Card -->
