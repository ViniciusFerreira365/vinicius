<!--Navbar -->
<head>
<nav class="mb-1 navbar navbar-expand-lg navbar-dark morpheus-den-gradient">
  <a class="navbar-brand" href="<?php echo base_url();?>">Menu-Principal</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>peopleC/sobre_api"> Sobre a API</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>peopleC/perguntas_api">Perguntas API</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>peopleC/comousar_apiT">Como usar a API no Terminal</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>peopleC/inseridado_api"> Inserir Dados API</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>peopleC/resultadoapi"> Resultado API</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="https://developers.google.com/people/">Documentação</a>
      </li>
  
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo base_url(); ?>peopleC/relatorio">Relatório</a>
      </li>
     

      
     
      
     
    </ul>
  </div>
</nav>
<!--/.Navbar -->