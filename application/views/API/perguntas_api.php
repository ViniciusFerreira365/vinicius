
<!-- Jumbotron -->
<div class="jumbotron-fluid my-5 text-center">

  <!-- Title -->
  <h2 class="card-title h2">Perguntas sobre API</h2>
  <!-- Subtitle -->
  <p class="blue-text my-4 font-weight-bold">Você deverá responder às seguintes perguntas</p>

  <!-- Grid row -->
  <div class="row d-flex justify-content-center">

    <!-- Grid column -->
    <div class="col-xl-5 pb-5">

      <p class="card">Quais são as principais características da API e suas finalidades?</p>
      <p class="card-text font-italic text font-italic">Resposta 1: Com esta API é possivel Listar e atualizar contatos por meio de uma conta google autenticada.</p>
      <p class="card">O quê é possível criar com o uso desta API?</p>
      <p class="card-text text font-italic">Resposta 2: É possivel criar uma agenda de contatos completa e sincroniza-la com sua conta google e ter essa agenda no celular, computador e outros </p>
      <p class="card">Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? E outras...</p>
      <p class="card-text text font-italic">Resposta 3: A API tem uma documentação agradavél, funções como utilizar e entre outros bem explicado, nao tem custo porem existe
      limite de cota regulando o uso da API, existem poucas restrições por utilizar REST via HTTP dificultou pouco a implementação em PHP, e por ter cotas acaba limitando seu uso. </p>
      <p class="card">Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?</p>
      <p class="card-text text font-italic">Resposta 4: Para a implementação é necessario ter uma conta google,criar chave de autorição e configurar autorização e consentimento OAuth2.0,
      baixar a biblioteca cliente, a varias opçoes a que eu escolhi foi via composer,
       e configurar arquivos no codeigniter.</p>
      <a href="https://developers.google.com/people/">Clique aqui para a Documentação</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-4">
</div>
<!-- Jumbotron -->