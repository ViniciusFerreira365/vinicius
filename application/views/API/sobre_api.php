<!-- Jumbotron -->
<div class="jumbotron p-0">

  <!-- Card image -->
  <div class="view overlay rounded-top">
    <img src="<?php echo base_url('assets/img/google1.png'); ?>" class="img-fluid rounded mx-auto d-block" alt="Sample image">
    <a>
    <div class="mask waves-effect rgba-white-slight"></div>
  </a>
  </div>

  <!-- Card content -->
  <div class="card-body text-center mb-3">

    <!-- Title -->
    <h3 class="card-title h3 my-4 font-weight-bold"><strong>Agenda IFSP</strong></h3>
    <!-- Text -->
    <p class="card-text text-dark">Seus Contatos em um unico lugar.</p>
    <p class="card-text text-justify text-xl-left">
      A agenda IFSP foi criada com o intuito de integrar todos seu contatos em um unico lugar!
    </p>
    <p class="card-text text-justify text-xl-left">
    A agenda funciona com a API People(Google) disponibilizada para o projeto, seu funcionamento é simples e rapido
    unificando todos seu contatos em um lugar so, com uma conta google autenticada você gerencia seus contatos no celular, 
    computador, tablet e muito mais!
    </p>

   <p class="note note-info text-justify"><strong>Nota de Informação:</strong>A API funciona em linha de comando ou na pagina PHP,
   no momento é apenas possivel apenas visualizar os contatos na pagina resultado API pelo fato da biblioteca cliente estar em desenvolvimento.  
  
   </p>
    <!-- Button -->
    <a href="resultadoapi" class="btn purple-gradient btn-rounded mb-4">Testar a API</a>

  </div>

</div>
<!-- Jumbotron -->
