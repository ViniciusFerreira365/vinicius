<?php
$service = new Google_Service_PeopleService($client);

    // Print the names for up to 10 connections.
    $optParams = array(
      'pageSize' => 10,
      'personFields' => 'names,emailAddresses',
    );
    $results = $service->people_connections->listPeopleConnections('people/me', $optParams);

    if (count($results->getConnections()) == 0) {
      print "No connections found.\n";
    } else {
      
      print "People:\n";
      foreach ($results->getConnections() as $person) {
        if (count($person->getNames()) == 0) {
          print "No names found for this connection\n";
        } else {
          $names = $person->getNames();
          $name = $names[0];
          printf("%s\n", $name->getDisplayName());
        }
      }
    }