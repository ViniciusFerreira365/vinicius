<?php
//Matriz dos objetos users,nome da classe mesmo do arquivo

class User{
    //Palavras private reservada impede o acesso externo aos atributos
    
    //Obrigatorios
    private $nome;
    private $sobrenome;
    private $senha;
    private $email;

    //Opcionais
    private $telefone;

    private $db;//CI linha 26

    function __construct($nome=null,$sobrenome=null,$email=null,$senha=null){
        //Constriuir server para fazer inicializacoes dos objetos
        //echo"construtor: $nome,$sobrenome,$email,$senha<br>"; //ver funcionando
        $this->nome  = $nome;
        $this->email = $email;
        $this->senha = $senha;
        $this->sobrenome = $sobrenome;

        //Acesso ao banco de dados pelo codigniter CI
        $ci = &get_instance();
        $this->db = $ci->db;
     


    }
    public function setTelefone($telefone){
        //echo"setter:$telefone<br>"; testar opcional
        $this->telefone = $telefone;
    }

    public function save(){
        //Criar banco em casa foto no celuluar
        $sql = "INSERT INTO user (nome,email,senha,sobrenome,telefone) VALUES ('$this->nome','$this->email','$this->senha','$this->sobrenome','$this->telefone')";
        $this->db->query($sql);//Executo variavel sql
    }

    /** *
     *  Obtem a lista de todos os usuarios cadastrados
     *@return associative array
    */
    public function getAll(){//ler tudo
        $sql = "SELECT * FROM user";
        $res = $this->db->query($sql);//resultado
        return $res->result_array();//Voltar em forma de vetor
    }

    public function getById($id){
        $rs = $this->db->get_where('user',"id = $id");
        //return $rs->result_array()[0];
        return $rs->row_array();//Devolve a linha sem precisar fazer uma matriz quando o resultado é unico
    }

        public function update($data,$id){
            $this->db->update('user', $data, "id = $id");
            echo $this->db->last_query();
            return $this->db->affected_rows();
        }

        public function delete($id){
            $this->db->delete('user',"id = $id");
        }
}


?>