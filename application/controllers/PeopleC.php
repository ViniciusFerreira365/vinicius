<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeopleC extends CI_Controller {

	
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('common/navbar');
		$this->load->view('common/card');
		$this->load->view('common/card1');
		$this->load->view('common/footer');
		}

		public function sobre_api(){
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('API/sobre_api');
			$this->load->view('common/footer');
		}

		public function perguntas_api(){
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('API/perguntas_api');
			$this->load->view('common/footer');
		}

		public function comousar_apiT(){
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('API/comousar_apiT');
			$this->load->view('common/footer');
		}

		public function inseridado_api()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('API/inseridado_api');
        $this->load->view('common/footer');
		}

		public function resultadoapi()
		{
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->model('PeopleModel');
			$data['client'] = $this->PeopleModel->getClient();;
			$this->load->view('API/resultadoapi',$data);
			$this->load->view('common/footer');
		}
		

		public function relatorio(){
			$this->load->view('common/header');
			$this->load->view('common/navbar');
			$this->load->view('API/relatorio_view');
			$this->load->view('common/footer');
		}

	

		
		
    
    
}
?>